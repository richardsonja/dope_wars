import {createAction, props, union} from '@ngrx/store';
import {HttpError} from '../../core/models/http_error.model';

export const recordHttpError = createAction('[HttpError] Record', props<{
  httpError: HttpError
}>());

const httpErrorActions = union({
  recordHttpError
});

export type httpErrorActionsUnion = typeof httpErrorActions;
