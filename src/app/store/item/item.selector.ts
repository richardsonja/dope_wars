import {createSelector} from '@ngrx/store';

import {IAppState} from '../app.state';
import {IItemState} from './item.state';

const itemState = (state: IAppState) => state.items;

export const selectItemList = createSelector(
  itemState,
  (state: IItemState) => state.items
);

export const selectItem = createSelector(
  itemState,
  (state: IItemState) => state.selectedItem
);
