import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {select, Store} from '@ngrx/store';
import {of} from 'rxjs/internal/observable/of';
import {catchError, map, switchMap, withLatestFrom} from 'rxjs/operators';

import {IAppState} from '../app.state';
import {getItem, getItemFailed, getItems, getItemsFailed, getItemsSuccess, getItemSuccess, itemActionsUnion} from './item.actions';
import {selectItemList} from './item.selector';
import {ListService} from '../../core/services/utilities/list.service';
import {Item} from '../../core/models/item.model';
import {ItemsService} from '../../core/services/items.service';
import {recordHttpError} from '../httperror/http-error.actions';

@Injectable()
export class ItemEffects {
  getItem$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getItem.type),
      map(action => action),
      withLatestFrom(this.store.pipe(select(selectItemList))),
      switchMap(([payload, items]) => {
        const selectedItem = items.filter(item => item.name === payload.name)[0];
        return of(getItemSuccess({item: selectedItem}));
      }),
      catchError((ex) => of(recordHttpError({httpError: {httpErrorResponse: ex}})))
      // catchError((ex) => of(getItemFailed({httpError: {httpErrorResponse: ex}})))
    ));

  getItems$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getItems.type),
      switchMap(() => this.itemService.getAll()),
      map((items: Item[]) => this.listService.shuffle(items)),
      switchMap((item: Item[]) => of(getItemsSuccess({items: item}))),
      catchError((ex) => of(recordHttpError({httpError: {httpErrorResponse: ex}})))
      // catchError((ex) => of(getItemsFailed({httpError: {httpErrorResponse: ex}})))
    ));

  recordItemFailed$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getItemsFailed.type,
        getItemFailed.type
      ),
      map(a => a.httpError),
      switchMap(httpError => of(recordHttpError({httpError})))
    ));

  constructor(
    private itemService: ItemsService,
    private listService: ListService,
    private actions$: Actions<itemActionsUnion>,
    private store: Store<IAppState>
  ) {
  }
}
