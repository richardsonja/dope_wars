import {Item} from '../../core/models/item.model';

export interface IItemState {
  items: Item[];
  selectedItem: Item;
}

export const initialItemState: IItemState = {
  items: [],
  selectedItem: null
};
