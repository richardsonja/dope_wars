import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import {IAppState} from '../app.state';
import {selectItem, selectItemList} from './item.selector';
import {getItem, getItems} from './item.actions';

@Injectable()
export class ItemFacade {
  allItems$ = this.store.select(selectItemList);
  selectedItems$ = this.store.select(selectItem);

  constructor(private store: Store<IAppState>) {
  }

  loadAll() {
    this.store.dispatch(getItems());
  }

  selectItem(name: string) {
    this.store.dispatch(getItem({name}));
  }
}
