import {createAction, props, union} from '@ngrx/store';
import {Item} from '../../core/models/item.model';
import {HttpError} from '../../core/models/http_error.model';

export const getItems = createAction('[Item] Get Items');
export const getItemsSuccess = createAction('[Item] Get Items Success', props<{ items: Item[] }>());
export const getItemsFailed = createAction('[Item] Get Items Failed', props<{ httpError: HttpError }>());

export const getItem = createAction('[Item] Get Item', props<{ name: string }>());
export const getItemSuccess = createAction('[Item] Get Item Success', props<{ item: Item }>());
export const getItemFailed = createAction('[Item] Get Item Failed', props<{ httpError: HttpError }>());

const itemActions = union({
  getItems,
  getItemsSuccess,
  getItemsFailed,
  getItem,
  getItemSuccess,
  getItemFailed
});

export type itemActionsUnion = typeof itemActions;
