import {IItemState, initialItemState} from './item.state';
import {getItemsSuccess, getItemSuccess} from './item.actions';
import {Action, createReducer, on} from '@ngrx/store';

export const reducer = createReducer(
  initialItemState,
  on(getItemsSuccess, (state, {items}) => ({
    ...state,
    items
  })),
  on(getItemSuccess, (state, {item}) => ({
    ...state,
    selectedItem: item
  }))
);

export function itemReducers(
  state: IItemState | undefined,
  action: Action) {
  return reducer(state, action);
}
