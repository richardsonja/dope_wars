import {ActionReducerMap} from '@ngrx/store';

import {routerReducer} from '@ngrx/router-store';
import {IAppState} from './app.state';
import {configReducers} from './config/config.reducers';
import {cityReducers} from './city/city.reducers';
import {endGameMessageReducers} from './endGameMessage/end-game-message.reducers';
import {itemReducers} from './item/item.reducers';
import {snackbarReducers} from './snackbar/snackbar.reducers';
import {playerReducers} from './player/player.reducers';
import {gameCenterReducers} from './gamecenter/game-center.reducers';

export const appReducers: ActionReducerMap<IAppState, any> = {
  router: routerReducer,
  snackbar: snackbarReducers,
  config: configReducers,
  cities: cityReducers,
  endGameMessages: endGameMessageReducers,
  items: itemReducers,
  players: playerReducers,
  gameCenters: gameCenterReducers
};
