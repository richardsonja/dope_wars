import {createSelector} from '@ngrx/store';

import {IAppState} from '../app.state';
import {IGameCenterState} from './game-center.state';

const GameCenterState = (state: IAppState) => state.gameCenters;

export const selectGameCenter = createSelector(
  GameCenterState,
  (state: IGameCenterState) => state.gameCenter
);
