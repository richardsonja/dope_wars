import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {catchError, map, switchMap, tap} from 'rxjs/operators';
import {of} from 'rxjs';

import {
  destroyGameCenter,
  GameCenterActionsUnion,
  getGameCenter,
  getGameCenterFailed,
  getGameCenterSuccess,
  setUpGameCenter,
  setUpGameCenterFailed,
  setUpGameCenterSuccess,
  updateGameCenter,
  updateGameCenterFailed,
  updateGameCenterSuccess
} from './game-center.actions';
import {recordHttpError} from '../httperror/http-error.actions';
import {Router} from '@angular/router';
import {GameCenter} from '../../core/models/game-center.model';
import {GameCenterService} from '../../core/services/game-center.service';
import {updatePlayerFailed, updatePlayerSuccess} from '../player/player.actions';
import {snackbarOpen} from '../snackbar/snackbar.actions';

@Injectable()
export class GameCenterEffects {
  getGameCenter$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getGameCenter.type),
      switchMap(() => of(this.gameCenterService.get())),
      switchMap((gameCenter: GameCenter) => of(getGameCenterSuccess({gameCenter}))),
      catchError((ex) => of(getGameCenterFailed({httpError: {httpErrorResponse: ex}})))
    ));

  setupGameCenterIfMissing$ = createEffect(() =>
    this.actions$.pipe(
      ofType(
        updatePlayerSuccess.type),
      switchMap(() => of(this.gameCenterService.get())),
      switchMap((gameCenter: GameCenter) => gameCenter
        ? of(setUpGameCenterSuccess({gameCenter}))
        : of(setUpGameCenter())),
      catchError((ex) => of(setUpGameCenterFailed({httpError: {httpErrorResponse: ex}})))
    ));

  setupGameCenter$ = createEffect(() =>
    this.actions$.pipe(
      ofType(
        setUpGameCenter.type,
        destroyGameCenter.type),
      switchMap(() => of(this.gameCenterService.createNew())),
      switchMap((gameCenter: GameCenter) => of(setUpGameCenterSuccess({gameCenter}))),
      catchError((ex) => of(setUpGameCenterFailed({httpError: {httpErrorResponse: ex}})))
    ));

  setUpGameCenterSuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(setUpGameCenterSuccess.type),
      tap(() => this.router.navigate(['/dashboard'])),
      catchError((ex) => of(updatePlayerFailed({httpError: {httpErrorResponse: ex}})))
    ), {
    dispatch: false
  });

  updateGameCenter$ = createEffect(() =>
    this.actions$.pipe(
      ofType(updateGameCenter.type),
      map(action => action.gameCenter),
      switchMap((gameCenter: GameCenter) =>
        this.gameCenterService.save(gameCenter).pipe(
          switchMap(() => of(updateGameCenterSuccess({gameCenter}))),
          catchError((ex) => of(updateGameCenterFailed({httpError: {httpErrorResponse: ex}})))
        )
      ),
      catchError((ex) => of(updateGameCenterFailed({httpError: {httpErrorResponse: ex}})))
    ));


  updateGameCenterSuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(updateGameCenterSuccess.type),
      tap(() => this.router.navigate(['/dashboard/'])),
      tap(() => snackbarOpen({message: 'Changes Saved'}))
    ), {
    dispatch: false
  });


  recordGameCenterFailed$ = createEffect(() =>
    this.actions$.pipe(
      ofType(updateGameCenterFailed.type,
        getGameCenterFailed.type,
        setUpGameCenterFailed.type
      ),
      map(a => a.httpError),
      switchMap(httpError => of(recordHttpError({httpError})))
    ));

  constructor(
    private gameCenterService: GameCenterService,
    private router: Router,
    private actions$: Actions<GameCenterActionsUnion>) {
  }
}
