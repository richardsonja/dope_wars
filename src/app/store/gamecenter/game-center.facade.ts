import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import {IAppState} from '../app.state';
import {selectGameCenter} from './game-center.selector';
import {destroyGameCenter, getGameCenter, setUpGameCenter, updateGameCenter} from './game-center.actions';
import {GameCenter} from '../../core/models/game-center.model';

@Injectable()
export class GameCenterFacade {
  gameCenter$ = this.store.select(selectGameCenter);

  constructor(private store: Store<IAppState>) {
  }

  load() {
    this.store.dispatch(getGameCenter());
  }

  update(gameCenter: GameCenter) {
    this.store.dispatch(updateGameCenter({gameCenter}));
  }

  setup() {
    this.store.dispatch(setUpGameCenter());
  }

  newGame() {
    this.store.dispatch(destroyGameCenter());
    this.store.dispatch(setUpGameCenter());
    this.store.dispatch(getGameCenter());
  }

  completeGame() {
    this.store.dispatch(destroyGameCenter());
    this.store.dispatch(setUpGameCenter());
    this.store.dispatch(getGameCenter());
  }

  abandomGame() {
    this.store.dispatch(destroyGameCenter());
    this.store.dispatch(setUpGameCenter());
    this.store.dispatch(getGameCenter());
  }

  destroy() {
    this.store.dispatch(destroyGameCenter());
  }
}
