import {getGameCenterSuccess, setUpGameCenterSuccess, updateGameCenterSuccess} from './game-center.actions';
import {IGameCenterState, initialGameCenterState} from './game-center.state';
import {Action, createReducer, on} from '@ngrx/store';

export const reducer = createReducer(
  initialGameCenterState,
  on(getGameCenterSuccess, (state, {gameCenter}) => ({
    ...state,
    gameCenter
  })),
  on(setUpGameCenterSuccess, (state, {gameCenter}) => ({
    ...state,
    gameCenter
  })),
  on(updateGameCenterSuccess, (state, {gameCenter}) => ({
    ...state,
    gameCenter
  }))
);

export function gameCenterReducers(
  state: IGameCenterState | undefined,
  action: Action) {
  return reducer(state, action);
}
