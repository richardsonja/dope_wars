import {GameCenter} from '../../core/models/game-center.model';

export interface IGameCenterState {
  gameCenter: GameCenter;
}

export const initialGameCenterState: IGameCenterState = {
  gameCenter: null
};
