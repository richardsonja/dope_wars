import {createAction, props, union} from '@ngrx/store';
import {HttpError} from '../../core/models/http_error.model';
import {GameCenter} from '../../core/models/game-center.model';

export const getGameCenter = createAction('[GameCenter] Get GameCenter');
export const getGameCenterSuccess = createAction('[GameCenter] Get GameCenter Success', props<{ gameCenter: GameCenter }>());
export const getGameCenterFailed = createAction('[GameCenter] Get GameCenter Failed', props<{ httpError: HttpError }>());

export const updateGameCenter = createAction('[GameCenter] Update GameCenter', props<{ gameCenter: GameCenter }>());
export const updateGameCenterSuccess = createAction('[GameCenter] Update GameCenter Success', props<{ gameCenter: GameCenter }>());
export const updateGameCenterFailed = createAction('[GameCenter] Update GameCenter Failed', props<{ httpError: HttpError }>());

export const setUpGameCenter = createAction('[GameCenter] Set Up GameCenter');
export const setUpGameCenterSuccess = createAction('[GameCenter] Set Up GameCenter Success', props<{ gameCenter: GameCenter }>());
export const setUpGameCenterFailed = createAction('[GameCenter] Set Up GameCenter Failed', props<{ httpError: HttpError }>());

export const destroyGameCenter = createAction('[GameCenter] Destroy GameCenter');

const GameCenterActions = union({
  getGameCenter,
  getGameCenterSuccess,
  getGameCenterFailed,
  updateGameCenter,
  updateGameCenterSuccess,
  updateGameCenterFailed,
  setUpGameCenter,
  setUpGameCenterSuccess,
  setUpGameCenterFailed,
  destroyGameCenter
});

export type GameCenterActionsUnion = typeof GameCenterActions;
