import {IEndGameMessageState, initialEndGameMessageState} from './end-game-message.state';
import {getEndGameMessagesSuccess, getEndGameMessageSuccess} from './end-game-message.actions';
import {Action, createReducer, on} from '@ngrx/store';

export const reducer = createReducer(
  initialEndGameMessageState,
  on(getEndGameMessagesSuccess, (state, {endGameMessages}) => ({
    ...state,
    endGameMessages
  })),
  on(getEndGameMessageSuccess, (state, {endGameMessage}) => ({
    ...state,
    selectedEndGameMessage: endGameMessage
  }))
);

export function endGameMessageReducers(
  state: IEndGameMessageState | undefined,
  action: Action) {
  return reducer(state, action);
}
