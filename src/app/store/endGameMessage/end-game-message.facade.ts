import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import {IAppState} from '../app.state';
import {selectEndGameMessage, selectEndGameMessageList} from './end-game-message.selector';
import {getEndGameMessage, getEndGameMessages} from './end-game-message.actions';

@Injectable()
export class EndGameMessageFacade {
  allEndOfGameMessages$ = this.store.select(selectEndGameMessageList);
  selectedEndOfGameMessages$ = this.store.select(selectEndGameMessage);

  constructor(private store: Store<IAppState>) {
  }

  loadAll() {
    this.store.dispatch(getEndGameMessages());
  }

  selectEndGameMessage(name: string) {
    this.store.dispatch(getEndGameMessage({name}));
  }
}
