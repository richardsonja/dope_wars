import {createSelector} from '@ngrx/store';

import {IAppState} from '../app.state';
import {IEndGameMessageState} from './end-game-message.state';

const endGameMessageState = (state: IAppState) => state.endGameMessages;

export const selectEndGameMessageList = createSelector(
  endGameMessageState,
  (state: IEndGameMessageState) => state.endGameMessages
);

export const selectEndGameMessage = createSelector(
  endGameMessageState,
  (state: IEndGameMessageState) => state.selectedEndGameMessage
);
