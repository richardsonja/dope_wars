import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {select, Store} from '@ngrx/store';
import {of} from 'rxjs/internal/observable/of';
import {catchError, map, switchMap, withLatestFrom} from 'rxjs/operators';

import {IAppState} from '../app.state';
import {
  endGameMessageActionsUnion,
  getEndGameMessage,
  getEndGameMessageFailed,
  getEndGameMessages,
  getEndGameMessagesFailed,
  getEndGameMessagesSuccess,
  getEndGameMessageSuccess
} from './end-game-message.actions';
import {selectEndGameMessageList} from './end-game-message.selector';
import {EndGameMessage} from '../../core/models/end_game_message';
import {EndGameMessageService} from '../../core/services/end_game_messages.service';
import {ListService} from '../../core/services/utilities/list.service';
import {recordHttpError} from '../httperror/http-error.actions';

@Injectable()
export class EndGameMessageEffects {
  getEndGameMessage$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getEndGameMessage.type),
      map(action => action),
      withLatestFrom(this.store.pipe(select(selectEndGameMessageList))),
      switchMap(([payload, EndGameMessages]) => {
        const selectedEndGameMessage = EndGameMessages.filter(item => item.name === payload.name)[0];
        return of(getEndGameMessageSuccess({endGameMessage: selectedEndGameMessage}));
      }),
      catchError((ex) => of(getEndGameMessageFailed({httpError: {httpErrorResponse: ex}})))
    ));

  getEndGameMessages$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getEndGameMessages.type),
      switchMap(() => this.endGameMessageService.getAll()),
      map((EndGameMessages: EndGameMessage[]) => this.listService.orderBy(EndGameMessages, true, 'maximumDollarAmount')),
      switchMap((EndGameMessages: EndGameMessage[]) => of(getEndGameMessagesSuccess({endGameMessages: EndGameMessages}))),
      catchError((ex) => of(getEndGameMessagesFailed({httpError: {httpErrorResponse: ex}})))
    ));

  recordEndGameMessageFailed$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getEndGameMessagesFailed.type,
        getEndGameMessageFailed.type
      ),
      map(a => a.httpError),
      switchMap(httpError => of(recordHttpError({httpError})))
    ));

  constructor(
    private endGameMessageService: EndGameMessageService,
    private listService: ListService,
    private actions$: Actions<endGameMessageActionsUnion>,
    private store: Store<IAppState>
  ) {
  }
}
