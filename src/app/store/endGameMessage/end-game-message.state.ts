import {EndGameMessage} from '../../core/models/end_game_message';

export interface IEndGameMessageState {
  endGameMessages: EndGameMessage[];
  selectedEndGameMessage: EndGameMessage;
}

export const initialEndGameMessageState: IEndGameMessageState = {
  endGameMessages: [],
  selectedEndGameMessage: null
};
