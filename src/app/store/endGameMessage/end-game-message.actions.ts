import {createAction, props, union} from '@ngrx/store';
import {EndGameMessage} from '../../core/models/end_game_message';
import {HttpError} from '../../core/models/http_error.model';

export const getEndGameMessages = createAction('[EndGameMessage] Get EndGameMessages');
export const getEndGameMessagesSuccess = createAction(
  '[EndGameMessage] Get EndGameMessages Success',
  props<{ endGameMessages: EndGameMessage[] }>());
export const getEndGameMessagesFailed = createAction(
  '[EndGameMessage] Get EndGameMessages Failed',
  props<{ httpError: HttpError }>());

export const getEndGameMessage = createAction(
  '[EndGameMessage] Get EndGameMessage',
  props<{ name: string }>());
export const getEndGameMessageSuccess = createAction(
  '[EndGameMessage] Get EndGameMessage Success',
  props<{ endGameMessage: EndGameMessage }>());
export const getEndGameMessageFailed = createAction(
  '[EndGameMessage] Get EndGameMessage Failed',
  props<{ httpError: HttpError }>());

const endGameMessageActions = union({
  getEndGameMessages,
  getEndGameMessagesSuccess,
  getEndGameMessagesFailed,
  getEndGameMessage,
  getEndGameMessageSuccess,
  getEndGameMessageFailed
});

export type endGameMessageActionsUnion = typeof endGameMessageActions;
