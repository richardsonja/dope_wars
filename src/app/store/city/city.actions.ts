import {createAction, props, union} from '@ngrx/store';
import {City} from '../../core/models/city.model';
import {HttpError} from '../../core/models/http_error.model';

export const getCities = createAction('[City] Get Cities');
export const getCitiesSuccess = createAction('[City] Get Cities Success', props<{ cities: City[] }>());
export const getCitiesFailed = createAction('[City] Get Cities Failed', props<{ httpError: HttpError }>());

export const getCity = createAction('[City] Get City', props<{ id: number }>());
export const getCitySuccess = createAction('[City] Get City Success', props<{ city: City }>());
export const getCityFailed = createAction('[City] Get City Failed', props<{ httpError: HttpError }>());

const cityActions = union({
  getCities,
  getCitiesSuccess,
  getCitiesFailed,
  getCity,
  getCitySuccess,
  getCityFailed
});

export type cityActionsUnion = typeof cityActions;
