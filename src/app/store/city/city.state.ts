import {City} from '../../core/models/city.model';

export interface ICityState {
  cities: City[];
  selectedCity: City;
}

export const initialCityState: ICityState = {
  cities: [],
  selectedCity: null
};
