import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {select, Store} from '@ngrx/store';
import {of} from 'rxjs/internal/observable/of';
import {catchError, map, switchMap, withLatestFrom} from 'rxjs/operators';

import {IAppState} from '../app.state';
import {cityActionsUnion, getCities, getCitiesFailed, getCitiesSuccess, getCity, getCityFailed, getCitySuccess} from './city.actions';
import {selectCityList} from './city.selector';
import {CityService} from '../../core/services/city.service';
import {City} from '../../core/models/city.model';
import {recordHttpError} from '../httperror/http-error.actions';

@Injectable()
export class CityEffects {
  getCity$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getCity.type),
      map(action => action),
      withLatestFrom(this.store.pipe(select(selectCityList))),
      switchMap(([id, Cities]) => {
        const selectedCity = Cities.filter(city => city.id === +id)[0];
        return of(getCitySuccess({city: selectedCity}));
      }),
      catchError((ex) => of(getCityFailed({httpError: {httpErrorResponse: ex}})))
    ));

  getCities$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getCities.type),
      switchMap(() => this.cityService.getAll()),
      switchMap((city: City[]) => of(getCitiesSuccess({cities: city}))),
      catchError((ex) => of(getCitiesFailed({httpError: {httpErrorResponse: ex}})))
    ));

  recordCityFailed$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getCitiesFailed.type,
        getCityFailed.type
      ),
      map(a => a.httpError),
      switchMap(httpError => of(recordHttpError({httpError})))
    ));

  constructor(
    private cityService: CityService,
    private actions$: Actions<cityActionsUnion>,
    private store: Store<IAppState>
  ) {
  }
}
