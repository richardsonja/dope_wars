import {getCitiesSuccess, getCitySuccess} from './city.actions';
import {ICityState, initialCityState} from './city.state';
import {Action, createReducer, on} from '@ngrx/store';

export const reducer = createReducer(
  initialCityState,
  on(getCitiesSuccess, (state, {cities}) => ({
    ...state,
    cities
  })),
  on(getCitySuccess, (state, {city}) => ({
    ...state,
    selectedCity: city
  }))
);

export function cityReducers(
  state: ICityState | undefined,
  action: Action) {
  return reducer(state, action);
}
