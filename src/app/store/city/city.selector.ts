import {createSelector} from '@ngrx/store';

import {IAppState} from '../app.state';
import {ICityState} from './city.state';

const cityState = (state: IAppState) => state.cities;

export const selectCityList = createSelector(
  cityState,
  (state: ICityState) => state.cities
);

export const selectSelectedCity = createSelector(
  cityState,
  (state: ICityState) => state.selectedCity
);
