import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import {getCities, getCity} from './city.actions';
import {selectCityList, selectSelectedCity} from './city.selector';
import {IAppState} from '../app.state';

@Injectable()
export class CityFacade {
  allCities$ = this.store.select(selectCityList);
  selectedCity$ = this.store.select(selectSelectedCity);

  constructor(private store: Store<IAppState>) {
  }

  loadAll() {
    this.store.dispatch(getCities());
  }

  selectCar(carId: number) {
    this.store.dispatch(getCity({id: carId}));
  }
}
