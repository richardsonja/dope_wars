import {RouterReducerState} from '@ngrx/router-store';

import {IConfigState, initialConfigState} from './config/config.state';
import {ICityState, initialCityState} from './city/city.state';
import {IEndGameMessageState, initialEndGameMessageState} from './endGameMessage/end-game-message.state';
import {IItemState, initialItemState} from './item/item.state';
import {initialSnackbarState, ISnackbarState} from './snackbar/snackbar.state';
import {initialPlayerState, IPlayerState} from './player/player.state';
import {IGameCenterState, initialGameCenterState} from './gamecenter/game-center.state';

export interface IAppState {
  router?: RouterReducerState;
  snackbar: ISnackbarState;
  config: IConfigState;
  cities: ICityState;
  endGameMessages: IEndGameMessageState;
  items: IItemState;
  players: IPlayerState;
  gameCenters: IGameCenterState;
}

export const initialAppState: IAppState = {
  snackbar: initialSnackbarState,
  config: initialConfigState,
  cities: initialCityState,
  endGameMessages: initialEndGameMessageState,
  items: initialItemState,
  players: initialPlayerState,
  gameCenters: initialGameCenterState
};

export function getInitialState(): IAppState {
  return initialAppState;
}
