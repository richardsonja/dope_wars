import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import {IAppState} from '../app.state';
import {MatSnackBarConfig} from '@angular/material';
import {snackbarOpen} from './snackbar.actions';

@Injectable()
export class SnackbarFacade {

  constructor(private store: Store<IAppState>) {
  }

  Open(message: string, action?: string, config?: MatSnackBarConfig) {
    this.store.dispatch(snackbarOpen({message, action, config}));
  }
}
