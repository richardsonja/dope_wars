import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import {IAppState} from '../app.state';
import {selectPlayer} from './player.selector';
import {getPlayer, updatePlayer} from './player.actions';
import {Player} from '../../core/models/player.model';

@Injectable()
export class PlayerFacade {
  player$ = this.store.select(selectPlayer);

  constructor(private store: Store<IAppState>) {
  }

  selectPlayer() {
    this.store.dispatch(getPlayer());
  }
  updatePlayer(player: Player) {
    this.store.dispatch(updatePlayer({player}));
  }
}
