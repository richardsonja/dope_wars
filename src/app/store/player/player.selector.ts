import {createSelector} from '@ngrx/store';

import {IAppState} from '../app.state';
import {IPlayerState} from './player.state';

const playerState = (state: IAppState) => state.players;

export const selectPlayer = createSelector(
  playerState,
  (state: IPlayerState) => state.player
);
