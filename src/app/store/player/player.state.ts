import {Player} from '../../core/models/player.model';

export interface IPlayerState {
  player: Player;
}

export const initialPlayerState: IPlayerState = {
  player: null
};
