import {getPlayerSuccess} from './player.actions';
import {initialPlayerState, IPlayerState} from './player.state';
import {Action, createReducer, on} from '@ngrx/store';

export const reducer = createReducer(
  initialPlayerState,
  on(getPlayerSuccess, (state, {player}) => ({
    ...state,
    player
  }))
);

export function playerReducers(
  state: IPlayerState | undefined,
  action: Action) {
  return reducer(state, action);
}
