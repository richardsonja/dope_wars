import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {catchError, map, switchMap} from 'rxjs/operators';
import {of} from 'rxjs';

import {
  getPlayer,
  getPlayerFailed,
  getPlayerSuccess,
  playerActionsUnion,
  updatePlayer,
  updatePlayerFailed,
  updatePlayerSuccess
} from './player.actions';
import {PlayerService} from '../../core/services/player.service';
import {Player} from '../../core/models/player.model';
import {recordHttpError} from '../httperror/http-error.actions';
import {Router} from '@angular/router';

@Injectable()
export class PlayerEffects {
  getPlayer$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getPlayer.type),
      switchMap(() => of(this.playerService.get())),
      switchMap((player: Player) => of(getPlayerSuccess({player}))),
      catchError((ex) => of(getPlayerFailed({httpError: {httpErrorResponse: ex}})))
    ));

  updatePlayer$ = createEffect(() =>
    this.actions$.pipe(
      ofType(updatePlayer.type),
      map(action => action.player),
      switchMap((player: Player) =>
        this.playerService.save(player).pipe(
          switchMap(() => of(updatePlayerSuccess({player}))),
          catchError((ex) => of(updatePlayerFailed({httpError: {httpErrorResponse: ex}})))
        )
      ),
      catchError((ex) => of(recordHttpError({httpError: {httpErrorResponse: ex}})))
    ));

  recordPlayerFailed$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getPlayerFailed.type,
        updatePlayerFailed.type
      ),
      map(a => a.httpError),
      switchMap(httpError => of(recordHttpError({httpError})))
    ));

  constructor(
    private playerService: PlayerService,
    private router: Router,
    private actions$: Actions<playerActionsUnion>) {
  }
}
