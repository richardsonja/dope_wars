import {createAction, props, union} from '@ngrx/store';
import {Player} from '../../core/models/player.model';
import {HttpError} from '../../core/models/http_error.model';

export const getPlayer = createAction('[Player] Get Player');
export const getPlayerSuccess = createAction('[Player] Get Player Success', props<{ player: Player }>());
export const getPlayerFailed = createAction('[Player] Get Player Failed', props<{ httpError: HttpError }>());

export const updatePlayer = createAction('[Player] Update Player', props<{ player: Player }>());
export const updatePlayerSuccess = createAction('[Player] Update Player Success', props<{ player: Player }>());
export const updatePlayerFailed = createAction('[Player] Update Player Failed', props<{ httpError: HttpError }>());

const playerActions = union({
  getPlayer,
  getPlayerSuccess,
  getPlayerFailed,
  updatePlayer,
  updatePlayerSuccess,
  updatePlayerFailed
});

export type playerActionsUnion = typeof playerActions;
