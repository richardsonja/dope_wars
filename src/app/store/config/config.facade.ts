import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import {IAppState} from '../app.state';
import {selectConfig} from './config.selector';
import {getConfig} from './config.actions';

@Injectable()
export class ConfigFacade {
  allConfigs$ = this.store.select(selectConfig);

  constructor(private store: Store<IAppState>) {
  }

  loadAll() {
    this.store.dispatch(getConfig());
  }
}
