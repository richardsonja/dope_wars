import {Injectable} from '@angular/core';
import {Actions, createEffect, Effect, ofType} from '@ngrx/effects';
import {catchError, map, switchMap} from 'rxjs/operators';
import {of} from 'rxjs';

import {configActionsUnion, getConfig, getConfigFailed, getConfigSuccess} from './config.actions';
import {EnvironmentService} from '../../core/services/environment.service';
import {recordHttpError} from '../httperror/http-error.actions';

@Injectable()
export class ConfigEffects {
  @Effect()
  getConfig$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getConfig.type),
      switchMap(() => this.environmentService.get()),
      switchMap((config: any) => of(getConfigSuccess(config))),
      catchError((ex) => of(getConfigFailed({httpError: {httpErrorResponse: ex}})))
    ));

  recordConfigFailed$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getConfigFailed.type,
      ),
      map(a => a.httpError),
      switchMap(httpError => of(recordHttpError({httpError})))
    ));

  constructor(
    private environmentService: EnvironmentService,
    private actions$: Actions<configActionsUnion>) {
  }
}
