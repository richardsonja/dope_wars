import {getConfigSuccess} from './config.actions';
import {IConfigState, initialConfigState} from './config.state';
import {Action, createReducer, on} from '@ngrx/store';

export const reducer = createReducer(
  initialConfigState,
  on(getConfigSuccess, (state, {config}) => ({
    ...state,
    selectedConfig: config
  }))
);

export function configReducers(
  state: IConfigState | undefined,
  action: Action) {
  return reducer(state, action);
}
