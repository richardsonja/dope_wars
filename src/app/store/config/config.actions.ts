import {createAction, props, union} from '@ngrx/store';
import {HttpError} from '../../core/models/http_error.model';

export const getConfig = createAction('[Config] Get Config');
export const getConfigSuccess = createAction('[Config] Get Config Success', props<{ config: any }>());
export const getConfigFailed = createAction('[Config] Get Config Failed', props<{ httpError: HttpError }>());

const configActions = union({
  getConfig,
  getConfigSuccess,
  getConfigFailed
});

export type configActionsUnion = typeof configActions;
