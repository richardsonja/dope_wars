export interface IConfigState {
  config: any;
}

export const initialConfigState: IConfigState = {
  config: null
};
