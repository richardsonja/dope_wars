import {getPlayerFailed, updatePlayerFailed} from './player/player.actions';
import {getItemFailed, getItemsFailed} from './item/item.actions';
import {getEndGameMessageFailed, getEndGameMessagesFailed} from './endGameMessage/end-game-message.actions';
import {getConfigFailed} from './config/config.actions';
import {getCitiesFailed, getCityFailed} from './city/city.actions';

export const failedActions = [
  getPlayerFailed.type,
  updatePlayerFailed.type,
  getItemsFailed.type,
  getItemFailed.type,
  getEndGameMessagesFailed.type,
  getEndGameMessageFailed.type,
  getConfigFailed.type,
  getCitiesFailed.type,
  getCityFailed.type,
];

