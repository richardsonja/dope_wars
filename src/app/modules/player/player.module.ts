import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PlayerRoutingModule} from './player-routing.module';
import {MaterialModule} from '../../core/material.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {PlayerSetUpComponent} from './set-up/set-up.component';
import {ReactiveFormsModule} from '@angular/forms';
import {PlayerFormUpComponent} from './set-up/form/player-form.component';
import {PlayerPreviousScoreComponent} from './set-up/previous-scores/previous-scores.component';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    PlayerRoutingModule
  ],
  exports: [],
  providers: [],
  declarations: [
    PlayerSetUpComponent,
    PlayerFormUpComponent,
    PlayerPreviousScoreComponent
  ]
})

export class PlayerModule {
}
