import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {Player} from '../../../../core/models/player.model';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-player-form',
  templateUrl: './player-form.component.html',
  styleUrls: ['./player-form.component.css']
})
export class PlayerFormUpComponent implements OnInit, OnChanges {
  @Input() player: Player;
  @Output() update: EventEmitter<Player> = new EventEmitter<Player>();

  playerForm: FormGroup;
  name = new FormControl('');

  constructor(
    private formBuilder: FormBuilder
  ) {
  }

  ngOnInit() {
    this.initForm(this.player);
  }

  ngOnChanges() {
    this.initForm(this.player);
  }

  private initForm(player: Partial<Player> = {}) {
    this.playerForm = this.formBuilder.group({
      name: [player.name, Validators.required]
    });
  }

  private updatePlayer(): void {
    const player = {
      ...this.player,
      ...this.playerForm.value
    };
    this.update.emit(player);
    this.initForm();
  }
}
