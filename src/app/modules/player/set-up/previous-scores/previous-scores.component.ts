import {Component, Input} from '@angular/core';
import {Player} from '../../../../core/models/player.model';

@Component({
  selector: 'app-previous-scores',
  templateUrl: './previous-scores.component.html',
  styleUrls: ['./previous-scores.component.css']
})
export class PlayerPreviousScoreComponent {
  @Input() player: Player;//= {} as Player;

}
