import {Component, OnInit} from '@angular/core';
import {Player} from '../../../core/models/player.model';
import {PlayerFacade} from '../../../store/player/player.facade';

@Component({
  selector: 'app-player-setup',
  templateUrl: './set-up.component.html',
  styleUrls: ['./set-up.component.css']
})
export class PlayerSetUpComponent implements OnInit {
  player$ = this.playerFacade.player$;

  constructor(
    private playerFacade: PlayerFacade
  ) {
  }

  ngOnInit() {
    this.playerFacade.selectPlayer();
  }

  public onUpdate(player: Player) {
    this.playerFacade.updatePlayer(player);
  }
}
