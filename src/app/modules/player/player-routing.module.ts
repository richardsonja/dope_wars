import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PlayerSetUpComponent} from './set-up/set-up.component';

const routes: Routes = [
  {
    path: 'player/setup',
    component: PlayerSetUpComponent
  },
  {
    path: 'player',
    component: PlayerSetUpComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlayerRoutingModule {
}
