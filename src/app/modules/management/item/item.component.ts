import {Component, OnInit} from '@angular/core';
import {select, Store} from '@ngrx/store';
import {IAppState} from '../../../store/app.state';
import {selectItemList} from '../../../store/item/item.selector';
import {getItems} from '../../../store/item/item.actions';
import {ItemFacade} from '../../../store/item/item.facade';

@Component({
  selector: 'app-mgt-items',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {
  items$ = this.itemFacade.allItems$;

  constructor(
    private itemFacade: ItemFacade
  ) {
  }

  ngOnInit() {
    this.itemFacade.loadAll();
  }
}
