import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CityComponent} from './city/city.component';
import {ManagementRoutingModule} from './management-routing.module';
import {ItemComponent} from './item/item.component';
import {EndGameMessagesComponent} from './end-game-message/end-game-messages.component';
import {MaterialModule} from '../../core/material.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {PriceRangeComponent} from './price-range/price-range.component';
import {EnvironmentComponent} from './environment/environment.component';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    FlexLayoutModule,
    ManagementRoutingModule
  ],
  exports: [
    PriceRangeComponent
  ],
  providers: [],
  declarations: [
    CityComponent,
    ItemComponent,
    EndGameMessagesComponent,
    PriceRangeComponent,
    EnvironmentComponent
  ]
})
export class ManagementModule {
}
