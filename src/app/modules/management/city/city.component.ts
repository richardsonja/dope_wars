import {Component, OnInit} from '@angular/core';
import {CityFacade} from '../../../store/city/city.facade';

@Component({
  selector: 'app-mgt-city',
  templateUrl: './city.component.html',
  styleUrls: ['./city.component.css']
})
export class CityComponent implements OnInit {
  cities$ = this.cityFacade.allCities$;

  constructor(
    private cityFacade: CityFacade
  ) {
  }

  ngOnInit() {
    this.cityFacade.loadAll();
  }
}
