import {Component, Input, OnInit} from '@angular/core';
import {PriceRange} from '../../../core/models/price_range.model';

@Component({
  selector: 'app-mgt-price-range',
  templateUrl: './price-range.component.html',
  styleUrls: ['./price-range.component.css']
})
export class PriceRangeComponent implements OnInit {
  @Input() priceRange: PriceRange;
  @Input() title: string;

  ngOnInit() {
  }
}
