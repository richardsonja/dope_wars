import {Component, OnInit} from '@angular/core';
import {EndGameMessageFacade} from '../../../store/endGameMessage/end-game-message.facade';

@Component({
  selector: 'app-mgt-eog',
  templateUrl: './end-game-messages.component.html',
  styleUrls: ['./end-game-messages.component.css']
})
export class EndGameMessagesComponent implements OnInit {
  endGameMessages$ = this.endGameMessageFacade.allEndOfGameMessages$;

  constructor(
    private endGameMessageFacade: EndGameMessageFacade
  ) {
  }

  ngOnInit() {
    this.endGameMessageFacade.loadAll();
  }
}
