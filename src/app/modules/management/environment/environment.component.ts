import {Component, OnInit} from '@angular/core';
import {IAppState} from '../../../store/app.state';
import {selectConfig} from '../../../store/config/config.selector';
import {select, Store} from '@ngrx/store';
import {getConfig} from '../../../store/config/config.actions';
import {ConfigFacade} from '../../../store/config/config.facade';

@Component({
  selector: 'app-mgt-city',
  templateUrl: './environment.component.html',
  styleUrls: ['./environment.component.css']
})
export class EnvironmentComponent implements OnInit {
  config$ = this.configFacade.allConfigs$;

  constructor(private configFacade: ConfigFacade) {
  }

  ngOnInit() {
    this.configFacade.loadAll();
  }
}
