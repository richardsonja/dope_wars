import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CityComponent} from './city/city.component';
import {ItemComponent} from './item/item.component';
import {EndGameMessagesComponent} from './end-game-message/end-game-messages.component';
import {AuthGuard} from '../../core/services/guards/guard.auth.service';
import {EnvironmentComponent} from './environment/environment.component';
import {PermissionGuard} from '../../core/services/guards/guard.permission.service';

const routes: Routes = [
  {
    path: 'mgt/city',
    component: CityComponent,
    canActivate: [AuthGuard, PermissionGuard],
    data: {
      permission: 'tbd',
      redirectTo: '/'
    }
  }, {
    path: 'mgt/item',
    component: ItemComponent,
    canActivate: [AuthGuard]
  }, {
    path: 'mgt/end-game-message',
    component: EndGameMessagesComponent,
    canActivate: [AuthGuard]
  }, {
    path: 'mgt/env',
    component: EnvironmentComponent,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManagementRoutingModule {
}
