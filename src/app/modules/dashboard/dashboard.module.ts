import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DashboardRoutingModule} from './dashboard-routing.module';
import {MaterialModule} from '../../core/material.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {ReactiveFormsModule} from '@angular/forms';
import {DashboardComponent} from './dashboard.component';
import {BuyListComponent} from './buy-list/buy-list.component';
import {SaleListComponent} from './sale-list/sale-list.component';
import {ManageItemComponent} from './item/manage-item.component';
import {PurchaseCounterComponent} from './item/purchase-counter/purchase-counter.component';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    DashboardRoutingModule
  ],
  exports: [],
  providers: [],
  declarations: [
    DashboardComponent,
    BuyListComponent,
    SaleListComponent,
    ManageItemComponent,
    PurchaseCounterComponent
  ]
})

export class DashboardModule {
}
