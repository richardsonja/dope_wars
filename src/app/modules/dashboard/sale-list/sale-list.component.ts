import {Component, Input} from '@angular/core';
import {GameCenter} from '../../../core/models/game-center.model';

@Component({
  selector: 'app-sale-list',
  templateUrl: './sale-list.component.html',
  styleUrls: ['./sale-list.component.css']
})
export class SaleListComponent {
  @Input() gameCenter: GameCenter;
}
