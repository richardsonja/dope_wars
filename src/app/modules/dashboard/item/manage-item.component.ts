import {Component, Input, OnInit} from '@angular/core';
import {Item} from '../../../core/models/item.model';
import {FormBuilder, FormGroup} from '@angular/forms';
import {createPurchaseCounterRangeValidator} from './purchase-counter/purchase-counter-range.validator';
import {CurrentItem} from '../../../core/models/current-item.model';

@Component({
  selector: 'app-manage-item',
  templateUrl: './manage-item.component.html',
  styleUrls: ['./manage-item.component.css']
})
export class ManageItemComponent implements OnInit {
  @Input() item: Item;
  @Input() currentItems: CurrentItem[];
  @Input() isBuyingItem: boolean;

  form: FormGroup;
  counterValue;
  minValue = 0;
  maxValue = 12;
  currentItemAvailable: CurrentItem = null;

  constructor(private fb: FormBuilder) {
  }

  ngOnInit() {
    this.form = this.fb.group({
      counter: [this.counterValue, createPurchaseCounterRangeValidator(this.maxValue, this.minValue)]
    });

    this.counterValue = !this.isBuyingItem && this.item.purchasedQuantity ? this.item.purchasedQuantity : 0;
    this.setCurrentItemIsAvailable();
    this.setMinAndMaxValues();
  }

  private setMinAndMaxValues() {
    if (!this.currentItemAvailable) {
      this.maxValue = this.counterValue;
      this.minValue = this.counterValue;
    }
  }

  private setCurrentItemIsAvailable() {
    this.currentItemAvailable = !this.currentItems
      ? null
      : Array.from(this.currentItems).filter((currentItem: CurrentItem) => currentItem.name === this.item.name)[0];
  }
}
