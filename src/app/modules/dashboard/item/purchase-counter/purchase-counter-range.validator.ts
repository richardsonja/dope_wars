import {FormControl} from '@angular/forms';

export function createPurchaseCounterRangeValidator(maxValue, minValue) {
  return (c: FormControl) => {
    const err = {
      rangeError: {
        given: c.value,
        max: maxValue || 10,
        min: minValue || 0
      }
    };

    return (c.value > +maxValue || c.value < +minValue) ? err : null;
  };
}
