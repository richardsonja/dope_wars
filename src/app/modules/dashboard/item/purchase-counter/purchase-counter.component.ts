import {Component, forwardRef, Input, OnChanges} from '@angular/core';
import {ControlValueAccessor, FormControl, NG_VALIDATORS, NG_VALUE_ACCESSOR} from '@angular/forms';
import {createPurchaseCounterRangeValidator} from './purchase-counter-range.validator';

@Component({
  selector: 'app-purchase-counter',
  templateUrl: './purchase-counter.component.html',
  styleUrls: ['./purchase-counter.component.css'],
  providers: [
    {provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => PurchaseCounterComponent), multi: true},
    {provide: NG_VALIDATORS, useExisting: forwardRef(() => PurchaseCounterComponent), multi: true}
  ]
})
export class PurchaseCounterComponent implements ControlValueAccessor, OnChanges {
  @Input('counterValue') theCounterValue = 0;
  @Input() counterRangeMax;
  @Input() counterRangeMin;

  get counterValue() {
    return this.theCounterValue;
  }

  set counterValue(val) {
    this.theCounterValue = val;
    this.propagateChange(val);
  }

  ngOnChanges(inputs) {
    if (inputs.counterRangeMax || inputs.counterRangeMin) {
      this.validateFn = createPurchaseCounterRangeValidator(this.counterRangeMax, this.counterRangeMin);
      this.propagateChange(this.counterValue);
    }
  }

  writeValue(value) {
    if (value) {
      this.counterValue = value;
    }
  }

  registerOnChange(fn) {
    this.propagateChange = fn;
  }

  registerOnTouched() {
  }

  increase() {
    this.counterValue++;
  }

  decrease() {
    this.counterValue--;
  }

  validate(c: FormControl) {
    return this.validateFn(c);
  }

  propagateChange: any = () => {
  };
  validateFn: any = () => {
  };
}
