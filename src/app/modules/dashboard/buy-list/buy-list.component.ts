import {Component, Input} from '@angular/core';
import {GameCenter} from '../../../core/models/game-center.model';

@Component({
  selector: 'app-buy-list',
  templateUrl: './buy-list.component.html',
  styleUrls: ['./buy-list.component.css']
})
export class BuyListComponent {
  @Input() gameCenter: GameCenter;
}
