import {Component, OnInit} from '@angular/core';
import {GameCenterFacade} from '../../store/gamecenter/game-center.facade';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  gameCenter$ = this.gameCenterFacade.gameCenter$;

  constructor(
    private gameCenterFacade: GameCenterFacade
  ) {
  }

  ngOnInit() {
    this.gameCenterFacade.load();
  }

}
