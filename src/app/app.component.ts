import {Component, OnInit} from '@angular/core';
import {IAppState} from './store/app.state';
import {select, Store} from '@ngrx/store';
import {selectGameCenter} from './store/gamecenter/game-center.selector';
import {getGameCenter} from './store/gamecenter/game-center.actions';
import {GameCenterFacade} from './store/gamecenter/game-center.facade';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit {
  title = 'The Party';
  gameCenter$ = this.gameCenterFacade.gameCenter$;

  constructor(private gameCenterFacade: GameCenterFacade) {
  }

  ngOnInit() {
    this.gameCenterFacade.load();
  }
}
