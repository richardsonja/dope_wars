import {Injectable} from '@angular/core';
import {City} from '../models/city.model';
import {Observable} from 'rxjs';
import {ApiService} from './infrastructure/api.service';


@Injectable()
export class CityService {
  constructor(
    private apiService: ApiService
  ) {
  }

  getAll(): Observable<City[]> {
    return this.apiService.get(`./assets/data/cities.json`);
  }

}
