import {Injectable} from '@angular/core';
import {Player} from '../models/player.model';
import {StorageService} from './infrastructure/storage.service';
import {MapperService} from './utilities/mapper.service';
import {Observable, of} from 'rxjs';


@Injectable()
export class PlayerService {
  private playerStorageString = 'userPlayer';

  constructor(
    private storageService: StorageService,
    private mapperService: MapperService
  ) {
  }

  get(): Player {
    const currentPlayer = this.storageService.get(this.playerStorageString);
    if (!currentPlayer) {
      return new Player();
    }

    return this.mapperService.deserializeObject(Player, currentPlayer);
  }

  save(player: Player): Observable<Player> {
    this.storageService.post(this.playerStorageString, player);
    return of(player);
  }

  destroy(): void {
    this.storageService.delete(this.playerStorageString);
  }
}
