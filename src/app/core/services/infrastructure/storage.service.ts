import {Injectable} from '@angular/core';

@Injectable()
export class StorageService {
  get(key: string): any {
    const localStorage = window.localStorage[key];
    return !localStorage ? localStorage : JSON.parse(localStorage);
  }

  getObject<T>(Clazz: new() => T, key: string): T {
    const localStorage = window.localStorage[key];
    return !localStorage ? new Clazz() : JSON.parse(localStorage);
  }

  post(key: string, body: any): void {
    window.localStorage[key] = JSON.stringify(body);
  }

  delete(key: string): void {
    window.localStorage.removeItem(key);
  }


}
