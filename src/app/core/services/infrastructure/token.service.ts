import {Injectable} from '@angular/core';
import {StorageService} from './storage.service';


@Injectable()
export class TokenService {
  private tokenString = 'userToken';

  constructor(
    private storageService: StorageService
  ) {
  }

  getToken(): string {
    return this.storageService.get(this.tokenString);
  }

  saveToken(token: string) {
    this.storageService.post(this.tokenString, token);
  }

  destroyToken() {
    this.storageService.delete(this.tokenString);
  }
}
