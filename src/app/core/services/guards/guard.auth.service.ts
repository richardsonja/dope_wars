import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';

import {PlayerService} from '../player.service';
import {GameCenterService} from '../game-center.service';
import {select, Store} from '@ngrx/store';
import {selectGameCenter} from '../../../store/gamecenter/game-center.selector';
import {IAppState} from '../../../store/app.state';
import {setUpGameCenter} from '../../../store/gamecenter/game-center.actions';

@Injectable()
export class AuthGuard implements CanActivate {
  gameCenter$ = this.store.pipe(select(selectGameCenter));

  constructor(
    private router: Router,
    private playerService: PlayerService,
    private gameCenterService: GameCenterService,
    private store: Store<IAppState>
  ) {
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    const player = this.playerService.get();
    if (!player || player.name == null) {
      this.router.navigate(['player/setup']);
      return false;
    }

    const gameCenter = this.gameCenterService.get();
    if (!gameCenter) {
      this.store.dispatch(setUpGameCenter());
    }

    return true;
  }
}
