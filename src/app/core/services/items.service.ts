import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

import {ApiService} from './infrastructure/api.service';
import {Item} from '../models/item.model';


@Injectable()
export class ItemsService {
  constructor(
    private apiService: ApiService
  ) {
  }

  getAll(): Observable<Item[]> {
    return this.apiService.get(`./assets/data/items.json`);
  }
}
