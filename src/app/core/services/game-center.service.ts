import {Injectable} from '@angular/core';
import {StorageService} from './infrastructure/storage.service';
import {GameCenter} from '../models/game-center.model';
import {ListService} from './utilities/list.service';
import {PlayerService} from './player.service';
import {environment} from '../../../environments/environment';
import {Item} from '../models/item.model';
import {City} from '../models/city.model';
import {MapperService} from './utilities/mapper.service';
import {select, Store} from '@ngrx/store';
import {selectCityList} from '../../store/city/city.selector';
import {IAppState} from '../../store/app.state';
import {selectItemList} from '../../store/item/item.selector';
import {forkJoin, Observable, of} from 'rxjs';
import {BaseService} from './infrastructure/base.service';
import {Player} from '../models/player.model';
import {ItemsService} from './items.service';
import {CityService} from './city.service';
import {takeUntil} from 'rxjs/operators';
import {CurrentItem} from '../models/current-item.model';
import {PriceRange} from '../models/price_range.model';


@Injectable()
export class GameCenterService extends BaseService {

  cities$ = this.store.pipe(select(selectCityList));
  items$ = this.store.pipe(select(selectItemList));
  private gameCenterStorageString = 'gameCenter';

  constructor(
    private storageService: StorageService,
    private listService: ListService,
    private store: Store<IAppState>,
    private playerService: PlayerService,
    private itemsService: ItemsService,
    private cityService: CityService,
    private mapperService: MapperService
  ) {
    super();
  }

  createNewGameCenter(player: Player, items: Item[], cities: City[]): GameCenter {
    const gameCenter = new GameCenter(
      player,
      items,
      cities,
      environment.playUntilNumberOfDays,
      environment.startingCashEquity,
      environment.startingRiskTolerance,
      environment.startingToolboxSize,
      environment.startingDebt);

    this.setRandomCity(gameCenter);

    this.save(gameCenter);
    return gameCenter;
  }

  createNew(): GameCenter {
    const player = this.playerService.get();
    let gameCenter: GameCenter;
    const seedData = forkJoin(this.cityService.getAll(), this.itemsService.getAll());
    seedData
      .pipe(takeUntil(this.unsubscribe))
      .subscribe(([cities, items]) => gameCenter = this.createNewGameCenter(player, items, cities));
    return gameCenter;
  }

  setRandomCity(gameCenter: GameCenter): GameCenter {
    const currentCity = gameCenter.currentCity;
    const filteredAvailableCities = currentCity
      ? gameCenter.availableCities.filter(x => x.name !== currentCity.name)
      : gameCenter.availableCities;
    return this.setCity(gameCenter, this.listService.shuffle(filteredAvailableCities)[0]);
  }

  get(): GameCenter {
    const game = this.storageService.get(this.gameCenterStorageString);
    return this.mapperService.deserializeObject(GameCenter, game);
  }

  save(gameCenter: GameCenter): Observable<GameCenter> {
    this.storageService.post(this.gameCenterStorageString, gameCenter);
    return of(gameCenter);
  }

  destroy(): void {
    this.storageService.delete(this.gameCenterStorageString);
  }

  private setCity(gameCenter: GameCenter, newCity: City): GameCenter {
    const maximumNumberOfItems = this.getMaximumNumberOfItems(gameCenter.availableCities);

    gameCenter.currentCity = newCity;
    gameCenter.currentItems = this.setRandomItems(gameCenter, maximumNumberOfItems);
    return gameCenter;
  }

  private setRandomItems(gameCenter: GameCenter, maximumNumberOfItems): CurrentItem[] {
    const currentMinimumNumberOfItems = gameCenter.currentCity.minimumNumberofItems
      ? Math.min(gameCenter.currentCity.minimumNumberofItems, maximumNumberOfItems)
      : maximumNumberOfItems;

    const currentMaximumNumberOfItems = gameCenter.currentCity.maximumNumberofItems
      ? Math.min(gameCenter.currentCity.maximumNumberofItems, maximumNumberOfItems)
      : maximumNumberOfItems;

    const randomNumber = this.getRandomNumber(currentMinimumNumberOfItems, currentMaximumNumberOfItems);

    const selectedItems: Item[] = this.listService
      .shuffle(gameCenter.availableItems)
      .slice(0, randomNumber);

    const randomlySelectedItem: Item = this.listService.shuffle(gameCenter.availableItems)[0];
    const priceRange: PriceRange = this.randomlySelectedPriceRange(randomlySelectedItem);

    return this.setCurrentItems(selectedItems, randomlySelectedItem, priceRange);
  }

  private randomlySelectedPriceRange(randomlySelectedItem: Item): PriceRange {
    const randomNumber = this.getRandomNumber(1, 100);
    if (randomNumber < 70) {
      return randomlySelectedItem.standardPrice;
    } else if (randomNumber >= 70 && randomNumber < 85) {
      return randomlySelectedItem.cheapPrice;
    } else {
      return randomlySelectedItem.expensivePrice;
    }
  }

  private getRandomNumber(min: number, max: number): number {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  private setCurrentItems(selectedItems: Item[], randomlySelectedItem: Item, randomlySelectedPriceRange: PriceRange): CurrentItem[] {
    return Array
      .from(selectedItems)
      .map((item: Item) => {
        const priceRange: PriceRange =
          (randomlySelectedItem.name === item.name
            && randomlySelectedPriceRange)
            ? randomlySelectedPriceRange
            : item.standardPrice;
        return this.createCurrentItem(item, priceRange);
      });
  }

  private createCurrentItem(item: Item, priceRange: PriceRange) {
    const theItem = new CurrentItem();
    theItem.name = item.name;
    theItem.price = this.getRandomNumber(priceRange.minimumPrice, priceRange.maximumPrice);
    theItem.messageCode = priceRange.messageCode;

    return theItem;
  }

  private adjustMaximumNumberOfItems(x: City, maximumNumberOfItems: number) {
    if (x.maximumNumberofItems
      || x.maximumNumberofItems < 1
      || x.maximumNumberofItems > maximumNumberOfItems) {
      x.maximumNumberofItems = maximumNumberOfItems;
    }

    return x;
  }

  private getMaximumNumberOfItems(cities: City[]) {
    return cities
      .map(x => x.maximumNumberofItems ? x.maximumNumberofItems : 0)
      .reduce((a, b) => Math.max(a, b));
  }
}
