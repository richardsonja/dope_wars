import {Injectable} from '@angular/core';
import {EnvironmentService} from '../environment.service';
import {EMPTY, Observable} from 'rxjs';
import {ApiService} from '../infrastructure/api.service';
import {EventMessage} from '../../models/event_message.model';
import {HttpErrorResponse} from '@angular/common/http';
import {BaseService} from '../infrastructure/base.service';
import {takeUntil} from 'rxjs/operators';


@Injectable()
export class EventLoggingService extends BaseService {
  private applicationName: string = null;

  constructor(private environmentService: EnvironmentService,
              private apiService: ApiService) {
    super();
    this.environmentService.get()
      .pipe(takeUntil(this.unsubscribe))
      .subscribe(x => this.applicationName = x.applicationName);
  }

  public parseErrorMessage(error: any): string {
    return error.message ? error.message : error.toString();
  }

  public logHttpErrorResponse(routePath: string, httpErrorResponse: HttpErrorResponse, userId: string = null): Observable<void> {
    const apiUrl = '/System/Caught/Error';
    return this.sendEvent(apiUrl, routePath, this.parseErrorMessage(httpErrorResponse), userId);
  }

  public logError(routePath: string, message: string, userId: string = null): Observable<void> {
    const apiUrl = '/Error/Is/Human';
    return this.sendEvent(apiUrl, routePath, message, userId);
  }

  public logWarn(routePath: string, message: string, userId: string = null): Observable<void> {
    const apiUrl = '/Warning/Will/Rober....';
    return this.sendEvent(apiUrl, routePath, message, userId);
  }

  public logInfo(routePath: string, message: string, userId: string = null): Observable<void> {
    const apiUrl = '/How/May/I/Help/You';
    return this.sendEvent(apiUrl, routePath, message, userId);
  }

  private sendEvent(apiUrl: string, routePath: string, message: string, userId: string): Observable<void> {
    if (!message) {
      console.warn('eventLoggingService -> ' + apiUrl + '-> ERROR MESSAGE IS EMPTY, no action taken');
      return EMPTY;
    }

    const eventMessage = new EventMessage();

    eventMessage.applicationName = this.applicationName;
    eventMessage.routePath = routePath;
    eventMessage.message = message;
    eventMessage.userId = userId;
    eventMessage.userAgent = navigator.userAgent;

    console.warn('eventLoggingService -> ' + apiUrl, eventMessage);

    // IMPLEMENT WHEN AVAILABLE
    return new Observable<void>();
  }
}
