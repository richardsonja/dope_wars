import {Injectable} from '@angular/core';

@Injectable()
export class ClazzService {
  constructor() {
  }

  naturalSort(elementA: string, elementB: string) {
    let filteredA;
    let filteredB;
    let shiftedFilteredA;
    let shiftedFilteredB;
    const regexAlphaNumeric = /-?(\d+)|(\D+)/g;
    const regexNumeric = /-?\d+/;
    filteredA = elementA.toLowerCase().match(regexAlphaNumeric);
    filteredA = this.isEmptyOrNull(filteredA)
      ? ''
      : filteredA;

    filteredB = elementB.toLowerCase().match(regexAlphaNumeric);
    filteredB = this.isEmptyOrNull(filteredB)
      ? ''
      : filteredB;


    while (filteredA.length && filteredB.length) {
      shiftedFilteredA = filteredA.shift();
      shiftedFilteredB = filteredB.shift();

      const isNumericA = regexNumeric.test(shiftedFilteredA);
      const isNumericB = regexNumeric.test(shiftedFilteredB);
      if (isNumericA || isNumericB) {
        if (!isNumericA || !isNumericB) {
          return !isNumericA
            ? 1
            : -1;
        }

        if (shiftedFilteredA !== shiftedFilteredB) {
          return shiftedFilteredA - shiftedFilteredB;
        }
      }

      return shiftedFilteredA === shiftedFilteredB
        ? 0
        : shiftedFilteredA > shiftedFilteredB
          ? 1
          : -1;
    }

    return filteredA.length - filteredB.length;
  }

  getProperty(clazz: any, properties: string []): any {
    if (this.isEmptyOrNull(clazz)) {
      return null;
    }

    return Array.from(
      properties
        .map(p => this.isEmptyOrNull(clazz[p]) ? null : clazz[p])
        .filter(p => !this.isEmptyOrNull(p))
    ).pop();
  }

  isDifferent(originalClazz: any, otherClazz: any, propertiesPath: string) {
    if (this.isEmptyOrNull(originalClazz)
      || this.isEmptyOrNull(otherClazz)
      || this.isEmptyOrNull(propertiesPath)) {
      return false;
    }

    const path = this.splitString(propertiesPath);

    const original = this.getProperty(originalClazz, path);
    const other = this.getProperty(otherClazz, path);

    return JSON.stringify(original) !== JSON.stringify(other);
  }

  isEmptyOrNull(value: any) {
    return (!value || value === undefined || value == null);
  }

  splitString(orderBy?: string, delimiter: string = '.'): string[] {
    return orderBy ? orderBy.split(delimiter) : null;
  }

  toLowerCaseString(value: any) {
    return value ? value.toString().toLowerCase().trim() : '';
  }
}
