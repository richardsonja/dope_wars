import {Injectable} from '@angular/core';
import {EndGameMessage} from '../models/end_game_message';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {ApiService} from './infrastructure/api.service';


@Injectable()
export class EndGameMessageService {
  constructor(
    private apiService: ApiService
  ) {
  }

  getAll(): Observable<EndGameMessage[]> {
    return this.apiService.get(`./assets/data/end_game_messages.json`)
      .pipe(map(data => data));
  }

}
