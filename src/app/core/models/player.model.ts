import {JsonObject, JsonProperty} from 'json2typescript';

@JsonObject('Player')
export class Player {
  @JsonProperty('name', String)
  name: string;

  @JsonProperty('previousScores', [String], true)
  previousScores: number[] = [];

  constructor() {
    this.name = void 0;
    this.previousScores = [];
  }
}
