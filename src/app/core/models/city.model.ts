export class City {
  id: number;
  name: string;
  displayName: string;
  numberOfCops: number;
  minimumNumberofItems: number;
  maximumNumberofItems: number;
}
