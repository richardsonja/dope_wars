import {PriceRange} from './price_range.model';

export class Item {
  name: string;
  purchasedQuantity = 0;
  standardPrice: PriceRange;
  cheapPrice: PriceRange;
  expensivePrice: PriceRange;
}
