export class PriceRange {
  minimumPrice: number;
  maximumPrice: number;
  messageCode: string;
}
