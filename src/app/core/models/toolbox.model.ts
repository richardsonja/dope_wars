import {Inventory} from './inventory.model';

export class Toolbox {
  numberOfTools: number;
  amountOfCashEquity: number;
  riskTolerance: number;
  inventoryOnHand: Inventory[];
  toolboxSize: number;

  constructor(
    startingCashEquity: number = void 0,
    startingRiskTolerance: number = void 0,
    startingToolboxSize: number = void 0) {
    this.amountOfCashEquity = startingCashEquity;
    this.riskTolerance = startingRiskTolerance;
    this.toolboxSize = startingToolboxSize;
    this.numberOfTools = 0;
    this.inventoryOnHand = [];
  }
}
