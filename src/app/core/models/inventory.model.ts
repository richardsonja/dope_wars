import {Item} from './item.model';

export class Inventory {
  item: Item;
  quality: number;
}
