export class Lender {
  debt: number;
  deposit: number;

  constructor(startingDebt: number = void 0) {
    this.debt = startingDebt;
    this.deposit = 0;
  }
}
