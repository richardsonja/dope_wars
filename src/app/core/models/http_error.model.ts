import {HttpErrorResponse} from '@angular/common/http';

export class HttpError {
  httpErrorResponse?: HttpErrorResponse;
  message?: string;
}
