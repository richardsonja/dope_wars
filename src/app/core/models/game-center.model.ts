import {Player} from './player.model';
import {Item} from './item.model';
import {City} from './city.model';
import {Lender} from './lender.model';
import {Toolbox} from './toolbox.model';
import {JsonObject} from 'json2typescript';
import {CurrentItem} from './current-item.model';

@JsonObject('GameCenter')
export class GameCenter {


  player: Player;
  availableItems: Item[];
  availableCities: City[];
  dayEndSetting: number;
  currentCity: City;
  currentItems: CurrentItem[];
  dayCounter: number;
  auditTrail: string[];
  lender: Lender;
  toolbox: Toolbox;

  constructor(
    player: Player = void 0,
    availableItems: Item[] = void 0,
    availableCities: City[] = void 0,
    dayEndSetting: number = void 0,
    startingCashEquity: number = void 0,
    startingRiskTolerance: number = void 0,
    startingToolboxSize: number = void 0,
    startingDebt: number = void 0) {
    this.player = player;
    this.availableItems = availableItems;
    this.availableCities = availableCities;
    this.dayCounter = 1;
    this.dayEndSetting = dayEndSetting;
    this.toolbox = new Toolbox(startingCashEquity, startingRiskTolerance, startingToolboxSize);
    this.lender = new Lender(startingDebt);
  }

  get IsEndOfGame() {
    return this.dayCounter >= this.dayEndSetting;
  }

  get theAvailableCities(): City[] {
    return Array.from(this.availableCities);
  }

  get theAvailableItems(): Item[] {
    return Array.from(this.availableItems);
  }

  get theDayEndSetting(): number {
    return this.dayEndSetting;
  }

  get thePlayer(): Player {
    return Object.assign(Object.create(Object.getPrototypeOf(this.player)), this.player);
  }
}
