export class EndGameMessage {
  name: string;
  maximumDollarAmount: number;
}
