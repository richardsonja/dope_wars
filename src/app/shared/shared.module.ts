import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FlexLayoutModule} from '@angular/flex-layout';
import {HeaderComponent} from './components/header/header.component';
import {MaterialModule} from '../core/material.module';
import {LayoutComponent} from './components/layout/layout.component';
import {RouterModule} from '@angular/router';
import {SideNavigationListComponent} from './components/sidenav/sidenav-list.component';
import {SubheaderComponent} from './components/subheader/subheader.component';
import {ConfirmationDialogComponent} from './components/confirmation-dialog/confirmation-dialog.component';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    FlexLayoutModule,
    RouterModule
  ],
  providers: [],
  declarations: [
    HeaderComponent,
    LayoutComponent,
    SubheaderComponent,
    SideNavigationListComponent,
    ConfirmationDialogComponent
  ],
  exports: [
    HeaderComponent,
    LayoutComponent,
    SubheaderComponent,
    SideNavigationListComponent,
    ConfirmationDialogComponent
  ],
  entryComponents: [
    ConfirmationDialogComponent
  ]
})
export class SharedModule {
}
