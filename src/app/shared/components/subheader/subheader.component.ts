import {Component, Input, OnInit} from '@angular/core';
import {GameCenter} from '../../../core/models/game-center.model';

@Component({
  selector: 'app-subheader',
  templateUrl: './subheader.component.html',
  styleUrls: ['./subheader.component.css']
})
export class SubheaderComponent implements OnInit {
  @Input() gameCenter: GameCenter;

  ngOnInit() {
  }
}
