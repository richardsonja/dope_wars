import {Component, EventEmitter, Output} from '@angular/core';
import {destroyGameCenter, getGameCenter, setUpGameCenter} from '../../../store/gamecenter/game-center.actions';
import {Router} from '@angular/router';
import {Store} from '@ngrx/store';
import {IAppState} from '../../../store/app.state';
import {GameCenterFacade} from '../../../store/gamecenter/game-center.facade';

@Component({
  selector: 'app-sidenav-list',
  templateUrl: './sidenav-list.component.html',
  styleUrls: ['./sidenav-list.component.css']
})
export class SideNavigationListComponent {
  @Output() sidenavClose = new EventEmitter();

  constructor(
    private router: Router,
    private gameCenterFacade: GameCenterFacade
  ) {
  }

  public onSidenavClose() {
    this.sidenavClose.emit();
  }

  public newGameCenter() {
    this.gameCenterFacade.newGame();
    this.router.navigate(['/dashboard']);
    this.onSidenavClose();
  }
}
