import {Component, Input} from '@angular/core';
import {Router} from '@angular/router';
import {MatDialog} from '@angular/material';
import {ConfirmationDialogComponent} from '../confirmation-dialog/confirmation-dialog.component';
import {GameCenter} from '../../../core/models/game-center.model';
import {GameCenterFacade} from '../../../store/gamecenter/game-center.facade';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {
  @Input() gameCenter: GameCenter;

  constructor(
    private router: Router,
    private gameCenterFacade: GameCenterFacade,
    private dialog: MatDialog
  ) {
  }

  onNewGameCenterClick(): void {
    if (this.gameCenter.dayEndSetting === this.gameCenter.dayCounter) {
      this.completeGameCenter();
    } else {
      const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
        width: '350px',
        data: 'Are you sure you want to abandon this game?'
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log('Yes clicked');
          this.abandonGameCenter();
        }
      });
    }
  }

  public completeGameCenter() {
    this.gameCenterFacade.completeGame();
    this.router.navigate(['/dashboard']);
  }

  public abandonGameCenter() {
    this.gameCenterFacade.abandomGame();
    this.router.navigate(['/dashboard']);
  }
}
