import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {ManagementModule} from './modules/management/management.module';
import {CoreModule} from './core/core.module';
import {HttpClientModule} from '@angular/common/http';
import {MaterialModule} from './core/material.module';

import {FlexLayoutModule} from '@angular/flex-layout';
import {SharedModule} from './shared/shared.module';
import {PlayerModule} from './modules/player/player.module';
import {ReactiveFormsModule} from '@angular/forms';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {environment} from '../environments/environment';
import {StoreRouterConnectingModule} from '@ngrx/router-store';
import {EffectsModule} from '@ngrx/effects';
import {StoreModule} from '@ngrx/store';
import {appReducers} from './store/app.reducers';
import {ConfigEffects} from './store/config/config.effects';
import {CityEffects} from './store/city/city.effects';
import {EndGameMessageEffects} from './store/endGameMessage/end-game-message.effects';
import {ItemEffects} from './store/item/item.effects';
import {SnackbarEffects} from './store/snackbar/snackbar.effects';
import {PlayerEffects} from './store/player/player.effects';
import {HttpErrorEffects} from './store/httperror/http-error.effects';
import {GameCenterEffects} from './store/gamecenter/game-center.effects';
import {DashboardModule} from './modules/dashboard/dashboard.module';
import {CityFacade} from './store/city/city.facade';
import {ConfigFacade} from './store/config/config.facade';
import {EndGameMessageFacade} from './store/endGameMessage/end-game-message.facade';
import {ItemFacade} from './store/item/item.facade';
import {PlayerFacade} from './store/player/player.facade';
import {ErrorFacade} from './store/httperror/http-error.facade';
import {SnackbarFacade} from './store/snackbar/snackbar.facade';
import {GameCenterFacade} from './store/gamecenter/game-center.facade';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    SharedModule,
    CoreModule,
    MaterialModule,
    ManagementModule,
    PlayerModule,
    DashboardModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    ///EffectsModule.forFeature() look it up
    StoreModule.forRoot(appReducers),
    EffectsModule.forRoot([
      SnackbarEffects,
      HttpErrorEffects,
      ConfigEffects,
      CityEffects,
      EndGameMessageEffects,
      ItemEffects,
      PlayerEffects,
      GameCenterEffects
    ]),
    StoreRouterConnectingModule.forRoot({stateKey: 'router'}),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    AppRoutingModule // must be imported as the last module as it contains the fallback route
  ],
  providers: [
    ErrorFacade,
    SnackbarFacade,
    CityFacade,
    ConfigFacade,
    EndGameMessageFacade,
    ItemFacade,
    PlayerFacade,
    GameCenterFacade
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
