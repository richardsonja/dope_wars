export const environment = {
  production: true,
  applicationName: 'The Party',
  playUntilNumberOfDays: 30,
  startingDebt: 5500,
  startingCashEquity: 2000,
  startingRiskTolerance: 100,
  startingToolboxSize: 100,
  serializerDebugMode: false
};
